<?php

require_once __DIR__ . '/vendor/autoload.php';

use Workerman\Lib\Timer;
use Workerman\Worker;

$connections = []; // сюда будем складывать все подключения

// Стартуем WebSocket-сервер на порту 27800
$worker = new Worker("websocket://0.0.0.0:27800");

$worker->onWorkerStart = static function($worker) use (&$connections)
{
    $interval = 5; // пингуем каждые 5 секунд
    Timer::add($interval, function() use(&$connections) {
        foreach ($connections as $connection) {
            // Если ответ не пришел 3 раза, то удаляем соединение из списка
            // и оповещаем всех участников об "отвалившемся" пользователе
            if ($connection->pingWithoutResponseCount >= 3) {
                unset($connections[$connection->id]);

                $messageData = [
                    'action' => 'ConnectionLost',
                    'userId' => $connection->id,
                    'userName' => $connection->userName,
                    'uuid' => $connection->uuid,
                ];
                $message = json_encode($messageData);

                $connection->destroy(); // уничтожаем соединение

                foreach ($connections as $cs) {
                    $cs->send($message);
                }
            }
            else {
//                $users = [];
//                foreach ($connections as $connect) {
//                    $users[] = [
//                        'userId' => $connect->id,
//                        'userName' => $connect->userName,
//                        'uuid' => $connect->uuid,
//                    ];
//                }
                $messageData = [
                    'action' => 'Ping',
                    'users' => ( $users ?? [] ),
                ];
                $message = json_encode($messageData);
                $connection->send($message);
                $connection->pingWithoutResponseCount++; // увеличиваем счетчик пингов
            }
        }
    });
};

$worker->onConnect = static function($connection) use(&$connections)
{
    // Эта функция выполняется при подключении пользователя к WebSocket-серверу
    $connection->onWebSocketConnect = static function($connection) use (&$connections)
    {
        // Достаём имя пользователя, если оно было указано
        if (!empty($_GET['userName'])) {
            $originalUserName = preg_replace('/[^a-zA-Zа-яА-ЯёЁ0-9\-\_ ]/u', '', trim($_GET['userName']));
        }
        else {
            $originalUserName = 'Инкогнито';
        }

        if (!empty($_GET['uuid'])) {
            $uuid = (string)$_GET['uuid'];
        }else {
            $uuid = 0;
        }

        // Проверяем уникальность имени в чате
        $userName = $originalUserName;

        $num = 2;
        do {
            $duplicate = false;
            foreach ($connections as $connect) {
                if ($connect->userName == $userName) {
                    $userName = "$originalUserName ($num)";
                    $num++;
                    $duplicate = true;
                    break;
                }
            }
        }
        while($duplicate);

        // Добавляем соединение в список
        $connection->userName = $userName;
        $connection->uuid = $uuid;
        $connection->pingWithoutResponseCount = 0; // счетчик безответных пингов

        $connections[$connection->id] = $connection;

        // Собираем список всех пользователей
        $users = [];
        foreach ($connections as $connect) {
            $users[] = [
                'userId' => $connect->id,
                'userName' => $connect->userName,
                'uuid' => $connect->uuid,
            ];
        }

        // Отправляем пользователю данные авторизации
        $messageData = [
            'action' => 'Authorized',
            'userId' => $connection->id,
            'userName' => $connection->userName,
            'uuid' => $connection->uuid,
            'users' => $users
        ];
        $connection->send(json_encode($messageData));

        // Оповещаем всех пользователей о новом участнике в чате
        $messageData = [
            'action' => 'Connected',
            'userId' => $connection->id,
            'userName' => $connection->userName,
            'uuid' => $connection->uuid,
        ];
        $message = json_encode($messageData);

        foreach ($connections as $connect) {
            $connect->send($message);
        }
    };
};

$worker->onClose = static function($connection) use(&$connections)
{
    // Эта функция выполняется при закрытии соединения
    if (!isset($connections[$connection->id])) {
        return;
    }

    // Удаляем соединение из списка
    unset($connections[$connection->id]);

    // Оповещаем всех пользователей о выходе участника из чата
    $messageData = [
        'action' => 'Disconnected',
        'userId' => $connection->id,
        'userName' => $connection->userName,
        'uuid' => $connection->uuid,
    ];
    $message = json_encode($messageData);

    foreach ($connections as $connect) {
        $connect->send($message);
    }
};

$worker->onMessage = static function($connection, $message) use (&$connections)
{
    $messageData = json_decode($message, true);
    $toUserId = isset($messageData['toUserId']) ? (int) $messageData['toUserId'] : 0;
    $action = isset($messageData['action']) ? $messageData['action'] : '';

    if ($action === 'Pong') {
        // При получении сообщения "Pong", обнуляем счетчик пингов
        $connection->pingWithoutResponseCount = 0;
    }else {
        // Дополняем сообщение данными об отправителе
        $messageData['userId'] = $connection->id;
        $messageData['userName'] = $connection->userName;
        $messageData['uuid'] = $connection->uuid;

        // Преобразуем специальные символы в HTML-сущности в тексте сообщения
        $messageData['text'] = htmlspecialchars($messageData['text']);
        // Заменяем текст заключенный в фигурные скобки на жирный
        $messageData['text'] = preg_replace('/\{(.*)\}/u', '<b>\\1</b>', $messageData['text']);

        if ($toUserId == 0) {
            // Отправляем сообщение всем пользователям
            $messageData['action'] = 'PublicMessage';
            foreach ($connections as $connect) {
                $connect->send(json_encode($messageData));
            }
        }else {
            $messageData['action'] = 'PrivateMessage';
            if (isset($connections[$toUserId])) {
                // Отправляем приватное сообщение указанному пользователю
                $connections[$toUserId]->send(json_encode($messageData));
                // и отправителю
                $connection->send(json_encode($messageData));
            }else {
                $messageData['text'] = 'Не удалось отправить сообщение выбранному пользователю';
                $connection->send(json_encode($messageData));
            }
        }
    }
};

Worker::runAll();