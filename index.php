<?php
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="/public/css/main.css">
</head>
<body>
<section class="nav bg-light mb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <a class="navbar-brand" href="/">Navbar</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Link</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
                                    Dropdown
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link disabled">Disabled</a>
                            </li>
                        </ul>
                        <form class="form-inline my-2 my-lg-0">
                            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                        </form>
                    </div>
                </nav>
            </div>
        </div>

    </div>
</section>

<div class="container">
    <div class="row">
        <div class="col-md-6 h-100">
            <div class="mb-2">
                <h2 class="user-name text-muted"></h2>
            </div>
            <div class="form-row user-form mb-2">
                <div class="col">
                    <input type="text" name="phone" class="form-control" placeholder="Логин" value="5555555552">
                </div>
                <div class="col">
                    <input type="text" name="password" class="form-control" placeholder="Пароль" value="777777">
                </div>
            </div>
            <div class="form-group chat-block">
                <form method="post" class="js-socket-form">
                    <div id="chat-result"></div>
                    <input type="text" class="form-control" id="message-input">
                </form>
            </div>
            <a class="btn btn-success js-socket-send">Отправить</a>
            <a class="btn btn-primary js-socket-open">Подключиться</a>
            <a class="btn btn-danger js-socket-close">Выйти</a>
        </div>
        <div class="col-md-6">
            <div class="list-group js-users-group"></div>
        </div>
    </div>
</div>

<script src="/resources/script/jquery.js"></script>
<script src="/resources/script/date-format.js"></script>
<script src="/resources/script/glob.js"></script>
<script src="/resources/script/main.js"></script>
</body>
</html>
