const mix = require('laravel-mix');
mix.styles([
    'resources/font/play/play.css',
    'resources/plugins/bootstrap/bootstrap.min.css',
    'resources/style/main.css',
], 'public/css/main.css');
mix.scripts([
    'resources/script/jquery.js',
    'resources/plugins/bootstrap/bootstrap.min.js',
    'resources/script/glob.js',
    'resources/script/date-format.js',
    'resources/script/main.js',
], 'public/js/main.js');
mix.copy('resources/font/play/font', 'public/font');