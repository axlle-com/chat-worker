let _currentUser = 0;
/***** ### user ### *****/
const _user_5555555552 = {
    login: '5555555552',
    password: '777777',
};
const _user_9997777777 = {
    login: '9997777777',
    password: '777777',
};
const _user_ = {
    login: '9621829550',
    password: '777777',
};

class User {
    token;
    instance;
    uuid;
    authUrl;
    login;
    password;
    name;

    constructor(object = null) {
        if (object) {
            this.login = object.login;
            this.password = object.password;
            this.authUrl = object.auth_url ? object.auth_url : 'https://testapi2.sanador.ru/api/rest/v1/login';
            this.auth();
        }
    }

    auth() {
        const self = this;
        const post = {
            'phone': this.login,
            'password': this.password,
            'action': this.authUrl
        };
        const request = new _glob.request(post);
        request.send((response) => {
            const data = request.getData();
            if (data) {
                self.token = data.access_token;
                self.uuid = data.uuid;
                self.name = data.username;
                self.login = '';
                self.password = '';
                _socket.init();
            }
        });
        return this;
    }

    show() {
        const self = this;
        $('.user-name').text(self.name);
        $('.user-form').hide();
    }

    clear() {
        $('.user-name').text('');
        $('.user-form').show();
    }
}

let _user;
/***** ### users ### *****/
const _users = {};
const _usersAll = {};
/***** ### socket ### *****/
const _socket = {
    client: {},
    onopen: function (event) {
        this.messageShow('Соединение установлено...');
        $('.js-socket-open').hide();
    },
    onerror: function (error) {
        this.messageShow(error.message ? error.message : 'Произошла ошибка');
    },
    onclose: function (event) {
        if (event.wasClean) {
            this.messageShow(`Соединение закрыто чисто, код=${event.code} причина=${event.reason ? event.reason : 'Инициировано пользователем'}`);
        } else {
            this.messageShow('Соединение прервано');
        }
        $('.js-socket-open').show();
        _currentUser = 0;
        _user.clear();
        this.clearUsers();
    },
    onmessage: function (event) {
        const self = this, el = $('#chat-result');
        let data = JSON.parse(event.data);
        console.log(data);
        if (data.action === 'authorized') {
            _user.instance = data.instance;
            _user.show();
            data.payload.data.forEach(function (item, i) {
                if (_user.instance == item.instance_id) {
                    item.messages = {};
                    _users[item.id] = item;
                }
                if (item.instance_id in _usersAll) {
                    _usersAll[item.instance_id][item.id] = item;
                } else {
                    _usersAll[item.instance_id] = {};
                    _usersAll[item.instance_id][item.id] = item;
                }
            });
            self.showUsers(true);
        }
        if (data.action === 'getMessages') {
            self.showChat(data.payload.data);
        }
        if(data.action === 'chats'){
            data.entities.forEach(function (item, i) {
                if (item.action === 'setMessage') {
                    let message = '', messageDef;
                    _users[_currentUser]['messages'][item.payload.id] = item.payload;
                    message += self.message(item.payload);
                    el.append(message);
                }
                if (item.action === 'updateLatestChatMessage') {
                    for (const key in item.payload) {
                        let el = data.payload[key];
                        let body = el.last_message ? '</br>Сообщение: ' + el.last_message : '';
                        let selector = `.js-user[data-id="${el.chat_id}"]`;
                        $(selector).find('.user-block-message').html(body);
                    }
                }
            });
        }
        if (data.action === 'updateStatusMessage') {
            // for (const key in data.payload){
            //     let el = data.payload[key];
            //     let body = el.last_message ? '</br>Сообщение: ' + el.last_message : '';
            //     let selector = `.js-user[data-id="${el.chat_id}"]`;
            //     $(selector).find('.user-block-message').html(body);
            // }
        }
        el.scrollTop(el.prop('scrollHeight'));
    },
    messageShow: function (text, name = 'Система') {
        const self = this, el = $('#chat-result');
        let message = `<div class="message-block">
                        <div class="chat-message">
                            <div class="user-name-chat">${name}</div>
                            <div>${text}</div>
                        </div>
                    </div>`;
        $('#chat-result').append(message);
        el.scrollTop(el.prop('scrollHeight'));
    },
    message: function (obj) {
        let now = new Date(obj.time * 1000).format('HH:MM:ss dd.mm.yyyy');
        return `<div class="message-block ${obj.from_me == 0 ? 'in' : 'out'}">
                        <div class="chat-message">
                            <div>${obj.body}</div>
                            <span>${now}</span>
                        </div>
                    </div>`;
    },
    messageSend: function () {
        const self = this;
        $('body').on('click', '.js-socket-send', function (e) {
            e.preventDefault();
            let input = $('#message-input');
            if (input.val() && self.client) {
                let data = {};
                data = {
                    action: 'systemData',
                    params: {
                        route: 'sendMessages',
                        chat_id: _currentUser,
                        body: input.val(),
                    },
                }
                self.client.send(JSON.stringify(data));
                input.val('');
            }
        })
        $('body').on('submit', '.js-socket-form', function (e) {
            e.preventDefault();
            let input = $('#message-input');
            if (input.val() && self.client) {
                let data = {};
                data = {
                    action: 'systemData',
                    params: {
                        route: 'sendMessages',
                        chat_id: _currentUser,
                        body: input.val(),
                    },
                }
                self.client.send(JSON.stringify(data));
                input.val('');
            }
        })
    },
    showUsers: function (isAuth = false) {
        const self = this;
        let string = '';
        for (let key in _users) {
            let body;
            if(isAuth){
                body = _users[key].unread_cnt ? '</br>Не прочитано: ' + _users[key].unread_cnt : '';
            }else{
                body = _users[key].last_messages ? '</br>Сообщение: ' + _users[key].last_messages : '';
            }
            let image = _users[key].image ? `<img src="https://trash.sanador.ru/${_users[key].image}" class="img-thumbnail">` : '';
            if (_currentUser !== key) {
                string += `<div type="button" class="list-group-item list-group-item-action js-user" data-id="${key}">
                            <div class="user-block-name">${image}${_users[key].name}</div>
                            <div class="user-block-message">${body}</div>
                            </div>`;
            } else {
                string += `<div type="button" class="list-group-item list-group-item-action active js-user" data-id="${key}">
                            <div class="user-block-name">${image}${_users[key].name}</div>
                            <div class="'user-block-message'">${body}</div>
                            </div>`;
            }
        }
        $('.js-users-group').html(string);
    },
    clearUsers: function () {
        $('.js-users-group').html('');
    },
    selectChat: function () {
        const self = this;
        $('body').on('click', '.js-user', function (e) {
            const el = $(this);
            const id = el.attr('data-id');
            if (_currentUser !== id) {
                $('.js-users-group').find('.js-user.active').removeClass('active');
                _currentUser = id;
                el.addClass('active');
                el.find('.user-block-message').text('');
                let data = {
                    action: 'systemData',
                    params: {
                        route: 'getMessages',
                        chat_id: _currentUser,
                    },
                }
                self.client.send(JSON.stringify(data));
            }
        });
    },
    showChat: function (payload) {
        const self = this;
        let message = '', el, messageDef;
        messageDef = `<div class="message-block">
                        <div class="chat-message">
                            <div>Переписка пуста</div>
                        </div>
                    </div>`;
        payload.data.forEach(function (item, i) {
            _users[_currentUser]['messages'][item.id] = item;
            message += self.message(item);
        });
        el = $('#chat-result');
        el.html(message ? message : messageDef);
        el.scrollTop(el.prop('scrollHeight'));
    },
    init: function () {
        const self = this;
        this.client = new WebSocket(`wss://testfb.sanador.ru:27801?token=${_user.token}`);
        this.client.onopen = function (event) {
            self.onopen(event);
        }
        this.client.onerror = function (error) {
            self.onerror(error);
        };
        this.client.onclose = function (event) {
            self.onclose(event);
        }
        this.client.onmessage = function (event) {
            self.onmessage(event);
        }
        for (const prop of Object.keys(_users)) {
            delete _users[prop];
        }
    },
    start: function () {
        const self = this;
        $('body').on('click', '.js-socket-open', function (e) {
            e.preventDefault();
            const userForm = $('.user-form');
            const login = userForm.find('[name="phone"]').val();
            const password = userForm.find('[name="password"]').val();
            _user = new User({login, password});
            $('#chat-result').html('');
        });
    },
    stop: function () {
        const self = this;
        $('body').on('click', '.js-socket-close', function (e) {
            e.preventDefault();
            self.client.close(1000, 'Конец связи');
            _currentUser = 0;
            _user.clear();
            self.clearUsers();
            $('#chat-result').html('');
        });
    },
    run: function () {
        this.start();
        this.stop();
        this.selectChat();
        this.messageSend();
    },
}

$(document).ready(function () {
    _glob.run();
    _socket.run();
});