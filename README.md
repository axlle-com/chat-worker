
---
### Первый запуск
1. Установка node.js `npm install npm@latest -g`
2. Упаковка js and css `npm run prod`
3. Установка workerman - в консоли command `composer update`
4. Запуск сервера в консоли command `php server.php start -d`
---
